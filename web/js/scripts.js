const slider = function() {
    let activeSlideIndex = 0;
    const slide = $( ".slider__item" );
    const showHideSlide = function(){

        slide.each(function(index) {
            let item = $(this);

            if(index === activeSlideIndex) {
                item.fadeIn();
            } else {
                item.fadeOut();
            }
        });
    };

    showHideSlide();

    $(".slider .arrow-right").click(function() {
        activeSlideIndex++;

        if(slide.length <= activeSlideIndex){
            activeSlideIndex = 0;
        }

        showHideSlide();
    });

    $(".slider .arrow-left").click(function() {
        activeSlideIndex--;

        if(activeSlideIndex < 0){
            activeSlideIndex = slide.length - 1;
        }

        showHideSlide();
    });
};

$(document).ready(function(){

    slider();

    const menuDropdown = $('#menu-dropdown');

    $('#menu').click(function (e) {
        menuDropdown.fadeToggle();
        e.stopPropagation();
    });

    $(document).click(function(event) {
        if(!$(event.target).closest('#menu-dropdown').length && menuDropdown.is(":visible")) {
            menuDropdown.fadeOut();
        }
    });
});
