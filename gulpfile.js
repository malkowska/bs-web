'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    useref = require('gulp-useref'),
    imagemin = require('gulp-imagemin'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano');

gulp.task('sass', function(){
    const plugins = [
        autoprefixer({browsers: ['last 1 version']}),
        cssnano()
    ];
    return gulp.src('web/scss/**/*.scss')
        .pipe(sass())
        .pipe(postcss(plugins))
        .pipe(gulp.dest('dist/css'))
});

gulp.task('jquery', function () {
    return gulp.src('./node_modules/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('web/js'));
});

gulp.task('scripts', gulp.series('jquery'), function(){
    return gulp.src('web/js/**/*.js')
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('useref', function(){
    return gulp.src('web/*.html')
        .pipe(useref())
        .pipe(gulp.dest('dist'))
});

gulp.task('images', function(){
    return gulp.src('web/img/**/*.+(png|jpg|gif|svg)')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
});

gulp.task('default', gulp.series('sass', 'scripts', 'useref', 'images'));
